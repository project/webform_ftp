<?php

namespace Drupal\webform_ftp\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'webform_ftp.settings';

  /**
   * @var \Drupal\Core\Database\Connection.
   */
  protected Connection $database;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, Connection $database) {
    parent::__construct($config_factory);
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'webform_ftp_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['webform_ftp_default_time'] = array(
      '#type' => 'select',
      '#title' => t('Webform submission upload to FTP period (default)'),
      '#options' => _webform_ftp_get_ftp_time_options(),
      '#default_value' => variable_get('webform_ftp_default_time', WEBFORM_FTP_NOT_UPLOAD),
      '#description' => t('Period runtime.'),
    );

    $form['webform_ftp_ftp_host'] = [
      '#type' => 'textfield',
      '#title' => t('HOST'),
      '#default_value' => variable_get('webform_ftp_ftp_host', ''),
      '#description' => t('Computer direction or other device providing data or services that a remote computer can access by means of a network.'),
    ];

    $form['webform_ftp_ftp_port'] = [
      '#type' => 'textfield',
      '#title' => t('PORT'),
      '#default_value' => variable_get('webform_ftp_ftp_port', '22'),
      '#description' => t('Number of port to connect to server. Defaults values: 21 or 22.'),
    ];

    $form['webform_ftp_ftp_user'] = [
      '#type' => 'textfield',
      '#title' => t('USER'),
      '#default_value' => variable_get('webform_ftp_ftp_user', ''),
      '#description' => t('User allowed to connect to the server.'),
    ];

    $form['webform_ftp_ftp_pass'] = [
      '#type' => 'password',
      '#title' => t('PASS'),
      '#post_render' => array('webform_ftp_hash'),
      '#description'   => t('Password of username. If you have already entered your password before, you should leave this field blank, unless you want to change the stored password.'),
    ];

    $form['webform_ftp_ftp_path'] = [
      '#type' => 'textfield',
      '#title' => t('PATH'),
      '#default_value' => variable_get('webform_ftp_ftp_path', ''),
      '#description' => t('Remote location folder'),
    ];

    $form = system_settings_form($form);
    $form['#submit'] = array_merge(['webform_ftp_password'], (array) $form['#submit']);

    return system_settings_form($form);
  }

  /**
   * Encrypt the server pass.
   */
  function webform_ftp_password($form, &$form_state): void {
    if (!empty($form_state['values']['webform_ftp_ftp_pass'])) {
      $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length(WEBFORM_FTP_ENCRYPTIONMETHOD));
      $form_state['values']['webform_ftp_ftp_pass'] = openssl_encrypt($form_state['values']['webform_ftp_ftp_pass'], WEBFORM_FTP_ENCRYPTIONMETHOD, WEBFORM_FTP_HASH_KEY, 0, $iv) . ':' . $iv;
    }
  }

  /**
   * Gets the "ftp time" dropdown options.
   */
  function _webform_ftp_get_ftp_time_options(): array {
    return array(
      WEBFORM_FTP_NOT_UPLOAD => t('Do not upload submissions to ftp'),
      24 * 60 * 60 => t('Upload submissions after 1 day'),
      7 * 24 * 60 * 60 => t('Upload submissions after @count days', array('@count' => 7)),
      30 * 24 * 60 * 60 => t('Upload submissions after @count days', array('@count' => 30)),
      1 => t('Upload submissions on each cron execution'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $ftp_time = $form_state['values']['ftp_time'];
    $nid = $form_state['values']['nid'];

    $this->database->delete('webform_ftp')
      ->condition('nid', $nid)
      ->execute();

    $this->database->insert('webform_ftp')
      ->fields([
        'nid' => $nid,
        'ftp_time' => $ftp_time
      ])->execute();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      static::SETTINGS,
    ];
  }

  public function validateForm(array &$form, FormStateInterface $form_state): void {
    if ($form_state['values']['webform_ftp_ftp_host'] == 1 && $form_state['values']['webform_ftp_ftp_host'] == '') {
      $form_state->setErrorByName('webform_ftp_ftp_host', t('You must enter an HOST'));
    }

    if ($form_state['values']['webform_ftp_ftp_port'] == 1 && $form_state['values']['webform_ftp_ftp_port'] == '') {
      $form_state->setErrorByName('webform_ftp_ftp_port', t('You must enter an PORT'));
    }

    if ($form_state['values']['webform_ftp_ftp_path'] == 1 && $form_state['values']['webform_ftp_ftp_path'] == '') {
      $form_state->setErrorByName('webform_ftp_ftp_path', t('You must enter an PATH'));
    }

    if (empty($form_state['values']['webform_ftp_ftp_user'])) {
      $form_state['values']['webform_ftp_ftp_pass'] = '';
    }
    elseif (empty($form_state['values']['webform_ftp_ftp_pass'])) {
      unset($form_state['values']['webform_ftp_ftp_pass']);
    }
  }

}
