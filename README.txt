INTRODUCTION
-----------
This module export submissions into csv file and upload the file to the 
folder on the ftp server that is configured

REQUIREMENTS
------------
This module requires the following modules:
 * Webform

INSTALLATION
------------
 * Copy the entire webform directory the Drupal sites/all/modules directory.
 * Login as an administrator. Enable the module in the "Administer" -> "Modules"
 * Go to permissions page and set permissions module

CONFIGURATION
-------------
Configure which Webforms utilize Webform FTP at
Configuration > Content Authoring > Webform FTP
or at admin/config/content/webform_ftp

DEVELOPMENT
-----------
Webform Hints was developed and is maintained by LUIGISA	

SUPPORT
-------
Please use the issue queue for filing bugs with this module at
https://www.drupal.org/sandbox/luigisa/2357941

MAINTAINERS
-----------
Current maintainers:
	Luigisa (biko) - https://www.drupal.org/user/1022312
